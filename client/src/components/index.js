import Welcome from "./Welcome";
import Navbar from "./Navbar";
import Loader from "./Loader";
import Services from "./Services";
import Transactions from "./Transactions";
import Footer from "./Footer";

export { Welcome, Navbar, Loader, Services, Transactions, Footer };
