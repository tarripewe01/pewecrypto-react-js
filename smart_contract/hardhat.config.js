// https://eth-ropsten.alchemyapi.io/v2/j91hUXpatduzRb7xXEV4gWO_IPUlcLWf

require("@nomiclabs/hardhat-waffle"); //Plugin to build smart-contract

module.exports = {
  solidity: "0.8.0",
  networks: {
    ropsten: {
      url: "https://eth-ropsten.alchemyapi.io/v2/j91hUXpatduzRb7xXEV4gWO_IPUlcLWf", // from alchemy
      accounts: [
        "bdb2780c62875c33afb47c115dca69fb9ce36dda1d67618e04811143282dde9f",
      ], // from metamask
    },
  },
};
